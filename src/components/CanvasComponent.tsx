import React from 'react';
import './CanvasComponent.css';
import Point from '../helper/Point';
import PointComponent from './PointComponent';

export interface CanvasState {
    array: Point[];
    movingPoint?: Point;
}

export class CanvasComponent extends React.Component<{}, CanvasState> {
    constructor(props: {}) {
        super(props);
        this.state = {
            array: [],
            movingPoint: undefined
        }
    }

    handleMouseDown(event: React.MouseEvent<HTMLDivElement>): void {
        this.setState({
            movingPoint: new Point(
                event.clientX - 15,
                event.clientY - 15)
        });
    }

    handleMouseMove(event: React.MouseEvent<HTMLDivElement>): void {
        if (this.state.movingPoint !== undefined) {
            this.state.movingPoint.x = event.clientX - 15;
            this.state.movingPoint.y = event.clientY - 15;
            this.setState({
                movingPoint: this.state.movingPoint
            });
        }
    }

    handleMouseUp(): void {
        if (this.state.movingPoint !== undefined) {
            this.setState({
                array: [
                    ...this.state.array, this.state.movingPoint
                ],
                movingPoint: undefined
            });
        }
    }

    render(): JSX.Element {
        return <div
            id="test"
            className="full-canvas"
            onMouseDown={this.handleMouseDown.bind(this)}
            onMouseMove={this.handleMouseMove.bind(this)}
            onMouseUp={this.handleMouseUp.bind(this)}
        >
            {this.state.array.map((point: Point): JSX.Element =>
                <PointComponent point={point} />)
            }
            {this.state.movingPoint && <PointComponent point={this.state.movingPoint} />}
        </div>;
    }
}

export default CanvasComponent;
