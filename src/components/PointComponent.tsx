import React from 'react';
import './PointComponent.css';
import Point from '../helper/Point';

export interface PointComponentProps {
    point: Point;
}

export class PointComponent extends React.Component<PointComponentProps, {}> {
    render(): JSX.Element {
        return <div
            key={this.props.point.x + this.props.point.y}
            className="square"
            style={{
                left: this.props.point.x,
                top: this.props.point.y
            }}>
        </div>;
    }
}

export default PointComponent;
