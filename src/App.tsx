import React from 'react';
import CanvasComponent from './components/CanvasComponent';

export class App extends React.Component {
    render(): JSX.Element {
        return <CanvasComponent/>;
    }
}

export default App;
